// OUR SERVICES
let services = [
    {
        name: "web design",
        img: function() {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization. Often many individuals will work in teams covering different aspects of the design process, although some designers will cover them all. The term 'web design' is normally used to describe the design process relating to the front-end (client side) design of a website including writing markup. Web design partially overlaps web engineering in the broader scope of web development. Web designers are expected to have an awareness of usability and if their role involves creating markup then they are also expected to be up to date with web accessibility guidelines."
    },
    {
        name: "graphic design",
        img: function () {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Graphic design is the process of visual communication and problem-solving through the use of typography, photography, iconography and illustration. The field is considered a subset of visual communication and communication design, but sometimes the term 'graphic design' is used synonymously. Graphic designers create and combine symbols, images and text to form visual representations of ideas and messages."
    },
    {
        name: "online support",
        img: function () {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Customer support is a range of customer services to assist customers in making cost-effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product. These services even may be done at the customer\'s side where he/she uses the product or service. In this case, it is called 'at home customer services' or 'at home customer support.'"
    },
    {
        name: "app design",
        img: function () {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Mobile app design encompasses both the user interface (UI) and user experience (UX). Designers are responsible for the overall style of the app, including things like the colour scheme, font selection, and the types of buttons and widgets the user will use."
    },
    {
        name: "online marketing",
        img: function () {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Digital marketing is the component of marketing that utilizes internet and online based digital technologies such as desktop computers, mobile phones and other digital media and platforms to promote products and services. Its development during the 1990s and 2000s, changed the way brands and businesses use technology for marketing. As digital platforms became increasingly incorporated into marketing plans and everyday life, and as people increasingly use digital devices instead of visiting physical shops, digital marketing campaigns have become prevalent, employing combinations of search engine optimization (SEO), search engine marketing (SEM)..."
    },
    {
        name: "seo service",
        img: function () {
            return `url(../images/${this.name.split(" ").join("-").toLowerCase()}.png)`
        },
        text: "Search engine optimization (SEO) is the process of growing the quality and quantity of website traffic by increasing the visibility of a website or a web page to users of a web search engine. SEO refers to the improvement of unpaid results (known as 'natural' or 'organic' results) and excludes direct traffic and the purchase of paid placement. Additionally, it may target different kinds of searches, including image search, video search, academic search, news search, and industry-specific vertical search engines. Promoting a site to increase the number of backlinks, or inbound links, is another SEO tactic. By May 2015, mobile search had surpassed desktop search."
    }
];

let tabs_services = document.querySelector(".services .tabs")
let tabs_works = document.querySelector(".works .tabs")
let img = document.querySelector(".services .tab-content .image-box");
let text = document.querySelector(".services .tab-content .tab-content-text");

function changeActive_1(array, content) {
    array.addEventListener("click", function (e) {
        [...this.children].forEach(e => {
            if (e.classList.contains("active")) {
                e.classList.remove("active");
            }
        })
        e.target.classList.add("active")
        content.forEach(el => {
            if (el.name.toLowerCase() === e.target.innerText.toLowerCase()) {
                img.style.backgroundImage = el.img();
                text.innerText = el.text
            }
        })
    });
}
changeActive_1(tabs_services, services);

// OUR WORK
function cardTemplate(id, url, text, category) {
    return `
			<div class="flip-card card-${id}">
				<div class="flip-card-inner">
					<div class="flip-card-front">
						<img src="${url}" alt="piece of work">
					</div>
					<div class="flip-card-back">
						<ul>
							<li class="icon link"></li>
							<li class="icon search"></li>
						</ul>
						<h2 class="flip-card-back__header">
							${text}
						</h2>
						<p class="flip-card-back__description">
							${category}
						</p>
					</div>
				</div>
			</div>
		`
}

let cards = [{
    id: 1,
    url: "./images/work-1.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 2,
    url: "./images/work-2.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 3,
    url: "./images/work-3.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 4,
    url: "./images/work-4.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 5,
    url: "./images/work-5.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 6,
    url: "./images/work-6.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 7,
    url: "./images/work-7.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 8,
    url: "./images/work-8.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 9,
    url: "./images/work-9.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 10,
    url: "./images/work-10.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 11,
    url: "./images/work-11.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 12,
    url: "./images/work-12.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 13,
    url: "./images/work-13.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 14,
    url: "./images/work-14.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 15,
    url: "./images/work-15.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 16,
    url: "./images/work-16.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 17,
    url: "./images/work-17.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 18,
    url: "./images/work-18.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 19,
    url: "./images/work-19.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 20,
    url: "./images/work-20.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 21,
    url: "./images/work-21.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 22,
    url: "./images/work-22.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 23,
    url: "./images/work-23.jpg",
    text: "Creative Design",
    category: "Web Design"

}]

window.onload = function () {
    loadCards(8);
}

function loadCards(n) {
     for (let i = 0; i < n; i++) {
         document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
     }
}

function changeActive_2(array, content) {
    array.addEventListener("click", function (e) {
        [...this.children].forEach(e => {
            if (e.classList.contains("active")) {
                e.classList.remove("active");
            }
        })
        e.target.classList.add("active");
        if(this.querySelector(".active").innerText === "All") {
            document.querySelector(".flip-cards").innerHTML = "";
            document.querySelector('button.load-more').style.display = "block";
            loadCards(8);
        } else {
            document.querySelector('button.load-more').style.display = "none";
            document.querySelector(".flip-cards").innerHTML = "";
            cards.forEach((el, i) => {
                if (el.category.toLocaleLowerCase() === e.target.innerText.toLowerCase()) {
                    document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
                }
            })
        }
    });
}

document.querySelector("button.load-more").onclick = function (e) {
    let list = [...document.querySelectorAll(".flip-cards .flip-card")];
    let counter = list.length;
    for (let i = counter; i < (counter + 8); i++) {
        if (i === cards.length) {
            document.querySelector('button.load-more').style.display = "none";
        } else {
            document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
        }
    }
}

changeActive_2(tabs_works)

// FEEDBACKS
let feedbacks = [
    {   
        id:1, 
        name: "Artemii Lebedev",
        position: "Artemii Levedev studio, CEO",
        feedback: "Yeah, it was nice and bla bla bla. Мы не обсуждаем дизайн, в отличие от многих. Мы обсуждаем: решена задача, которая перед нами была поставлена, или не решена.",
        img: function() {
            return `./images/${this.name.toLowerCase().split(" ").join("-")}.jpg`
        },
    },
    {   
        id:2, 
        name: "Richard Hendricks",
        position: "Pied Pipper, CEO",
        feedback: "Look, guys, for thousands of years, guys like us gave gotten the s*** kicked out of us. But now, for the first time, we’re living in an era, where we can be in charge and build empires. We could be the vikings of our day.",
        img: function () {
            return `./images/${this.name.toLowerCase().split(" ").join("-")}.jpg`
        },
    },
    {   
        id:3,
        name: "Erlich Bachman",
        position: "Founder of Aviato",
        feedback: "Today's user wants access to all their files, from all of their devices, instantly. That's why cloud-based is the Holy Grail. Now Dropbox is winning. But when it comes to audio and video files, they might as well be called Dripbox.",
        img: function () {
            return `./images/${this.name.toLowerCase().split(" ").join("-")}.jpg`
        },
    },
    {   
        id: 4,
        name: "Gavin Belson",
        position: "Hooli, CEO",
        feedback: "It’s weird. They always travel in groups of five. These programmers, there’s always a tall, skinny white guy; short, skinny Asian guy; fat guy with a ponytail; some guy with crazy facial hair; and then an East Indian guy. It’s like they trade guys until they all have the right group.",
        img: function () {
            return `./images/${this.name.toLowerCase().split(" ").join("-")}.jpg`
        },
    }
];

showFeedback(feedbacks[2])

function showFeedback(client) {
    document.querySelector(".feedbacks .feedback-box__text").innerText = client.feedback;
    document.querySelector(".feedbacks .feedback-box__author").innerText = client.name;
    document.querySelector(".feedbacks .feedback-box__author-position").innerText = client.position;
    document.querySelector(".feedbacks .feedback-box__image").style.backgroundImage = `url(${client.img()})`;
}

[...document.querySelectorAll(".client")].forEach(e => {
    
    feedbacks.forEach(elem => {
        if (elem.id == e.classList[1].slice(-1)) {
            console.log(true);
            e.style.backgroundImage = `url(${elem.img()}`;
        }
    })

    e.onclick = function () {
        [...document.querySelectorAll(".client")].forEach((e) => {
            if (e.classList.contains("active")) {
                e.classList.remove("active");
            }
        })
        e.classList.add("active");
        let n = e.classList[1].slice(-1);
        feedbacks.forEach((elem, index) => {
            // console.log(elem)
            if(elem.id == n) {
                showFeedback(elem)
            }
        })
    }
})

let left = document.querySelector(".left"),
    right = document.querySelector(".right");

function slider(arrow) {
    arrow.addEventListener("click", function (e) {
        let n;
        [...document.querySelectorAll(".client")].forEach((e) => {
            if (e.classList.contains("active")) {
                e.classList.remove("active");
                n = Number(e.classList[1].slice(-1))
                return n
            }
        })
        function slide() {
            document.querySelector(`.client.client_${n}`).classList.add("active")
            showFeedback(feedbacks[n - 1]);
        }
        if (arrow === right) {
            if (n < 4) {
                n++
                slide()
            } else if (n == 4) {
                n = 1
                slide()
            }
        } else {
            if (n > 1) {
                n--
                slide()
            } else if (n == 1) {
                n = 4
                slide()
            }
        }
    });
}
slider(left)
slider(right)