function cardTemplate(id, url, text, category) {
    return `
			<div class="flip-card card-${id}">
				<div class="flip-card-inner">
					<div class="flip-card-front">
						<img src="${url}" alt="piece of work">
					</div>
					<div class="flip-card-back">
						<ul>
							<li class="icon link"></li>
							<li class="icon search"></li>
						</ul>
						<h2 class="flip-card-back__header">
							${text}
						</h2>
						<p class="flip-card-back__description">
							${category}
						</p>
					</div>
				</div>
			</div>
		`
}

let cards = [{
    id: 1,
    url: "./images/work-1.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 2,
    url: "./images/work-2.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 3,
    url: "./images/work-3.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 4,
    url: "./images/work-4.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 5,
    url: "./images/work-5.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 6,
    url: "./images/work-6.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 7,
    url: "./images/work-7.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 8,
    url: "./images/work-8.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 9,
    url: "./images/work-9.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 10,
    url: "./images/work-10.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 11,
    url: "./images/work-11.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 12,
    url: "./images/work-12.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 13,
    url: "./images/work-13.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 14,
    url: "./images/work-14.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 15,
    url: "./images/work-15.jpg",
    text: "Creative Design",
    category: "Wordpress"

}, {
    id: 16,
    url: "./images/work-16.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 17,
    url: "./images/work-17.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 18,
    url: "./images/work-18.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 19,
    url: "./images/work-19.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 20,
    url: "./images/work-20.jpg",
    text: "Creative Design",
    category: "Graphic Design"

}, {
    id: 21,
    url: "./images/work-21.jpg",
    text: "Creative Design",
    category: "Landing Pages"

}, {
    id: 22,
    url: "./images/work-22.jpg",
    text: "Creative Design",
    category: "Web Design"

}, {
    id: 23,
    url: "./images/work-23.jpg",
    text: "Creative Design",
    category: "Web Design"

}]

window.onload = function () {
    for (let i = 0; i < 4; i++) {
        document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
    }
}

document.querySelector("button.load-more").onclick = function (e) {
    let list = [...document.querySelectorAll(".flip-cards .flip-card")];
    let counter = list.length;
    for (let i = counter; i < (counter + 8); i++) {
        if (i === cards.length) {
            document.querySelector('button.load-more').style.display = "none";
        } else {
            document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
        }
    }
}

// TAB CLICK
document.querySelector(".works .tabs").addEventListener("click", function (e) {
    // console.log(e.target);

    [...document.querySelectorAll(".works .tabs .tab")].forEach(elem => {
        elem.classList.remove("active");
    })
    e.target.classList.add("active");

    if (e.target.innerText.toLowerCase() !== "all") {
        [...document.querySelectorAll(".flip-card")].forEach(elems => {
            elems.remove();
        })
        // console.log(e.target.innerText.toLowerCase())
        for (const iterator of cards) {
            if (e.target.innerText.toLowerCase() === iterator.category.toLowerCase()) {
                document.querySelector(".flip-cards").innerHTML += cardTemplate(iterator.id, iterator.url, iterator.text, iterator.category)
            }
        }
        document.querySelector('button.load-more').style.display = "none";

    } else {
        [...document.querySelectorAll(".flip-card")].forEach(elems => {
            elems.remove();
        })
        for (let i = 0; i < 4; i++) {
            document.querySelector(".flip-cards").innerHTML += cardTemplate(cards[i].id, cards[i].url, cards[i].text, cards[i].category)
        }
        document.querySelector('button.load-more').style.display = "block";
    }

});